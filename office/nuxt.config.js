module.exports = {
    ssr: false,
    target: 'static',
    head: {
        title: "Todos",
        meta: [
            { charset: "utf-8" },
            { name: "viewport", content: "width=device-width, initial-scale=1" },
            {
                hid: "description",
                name: "description",
                content: "Todos Applicaton"
            }
        ],
        script: []
    },
    loading: { color: "#3adced" },
    css: [
        "~assets/css/app", "~assets/css/theme"
    ],
    publicRuntimeConfig: {
        remoteBaseUrl: process.env.REMOTE_BASE_URL,
    },
    plugins: [
        '~/plugins/common.js',
        '~/plugins/axios.js',
        '~/plugins/i18n.js',
        '~/plugins/external-packages.js',
        '~/plugins/vue-tel-input.js',
        //plugin call also can write this way rendering only client-side
        { src: '~/plugins/apexcharts.js', mode: 'client'},
    ],
    modules: [
        '@nuxtjs/axios',
        'nuxt-i18n',
        '@nuxtjs/toast',
        '@nuxtjs/auth-next',
        ['vue-scrollto/nuxt', { duration: 300 }],
    ],
    buildModules: [
        "@nuxtjs/vuetify",
        '@nuxtjs/dotenv',
        '@nuxtjs/moment',
    ],
    router: {
        middleware: ['auth']
    },
    vuetify: {
        
    },
    axios: {
        credentials: true
    },
    i18n: {
        locales: [
            {
                name: 'English',
                code: 'en',
                iso: 'en-US',
                flag: 'gb.png',
                file: 'en.js'
            },
            // {
            //     name: 'Italiano',
            //     code: 'it',
            //     iso: 'it-IT',
            //     flag: 'it.png',
            //     file: 'it.js'
            // }
        ],
        lazy: true,
        langDir: 'lang/',
        defaultLocale: 'en',
        detectBrowserLanguage: {
            useCookie: false,
            cookieKey: 'i18n_redirected',
            onlyOnRoot: true,  // recommended
            cookieCrossOrigin: true
        }
    },
    toast: {
        position: 'top-center',
        duration: 5000
    },
    auth: {
        redirect: {
            login: '/auth/login',
            logout: '/auth/login',
            callback: '/auth/login',
            home: '/'
        },
        fullPathRedirect: true,
        strategies: {
            local: {
                token: {
                    property: 'token',
                    required: true,
                    type: 'Bearer',
                    maxAge: 86400, //24 hrs
                    global: true,
                },
                user: {
                    property: 'user',
                    autoFetch: true,
                },
                endpoints: {
                    login: { url: '/v1/auth/login', method: 'post' },
                    logout: { url: '/v1/auth/logout', method: 'post' },
                    user: { url: '/v1/auth/me', method: 'get' },
                }
            }
        },
        plugins: ['~/plugins/auth.js']
    }
};