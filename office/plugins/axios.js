export default function ({ app, $axios, $config }) {
    $axios.setBaseURL($config.remoteBaseUrl + '/api/' + app.i18n.locale);
}
