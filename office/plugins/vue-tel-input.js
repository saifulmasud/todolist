import Vue from 'vue';
import vuetify from "vuetify";
import 'vuetify/dist/vuetify.min.css'
import VueTelInputVuetify from 'vue-tel-input-vuetify/lib';

Vue.use(VueTelInputVuetify, {
  vuetify,
});