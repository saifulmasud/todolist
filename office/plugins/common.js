export default ({ app, $axios }, inject) => {
    inject('getValidationError', (errors, key) => {
        if (errors == null) {
            return null;
        } else if (errors[key]) {
            return errors[key].join();
        } else {
            return "";
        }
    });
    inject('autoTranslate', async (translations, keysToBeTranslated) => {
        let transObjectWith = {};
        keysToBeTranslated.forEach(keyToBeTranslated => {
            let foundInsertedObject = translations.find(translation => translation[keyToBeTranslated]);
            transObjectWith[keyToBeTranslated] = foundInsertedObject ? foundInsertedObject[keyToBeTranslated] : null;
        });
        let translationsToDo = [];
        translations.forEach((translation) => {
            let translationToDo = {};
            keysToBeTranslated.forEach(keyToBeTranslated => {
                if (!translation[keyToBeTranslated] && transObjectWith[keyToBeTranslated]) {
                    translationToDo[keyToBeTranslated] = transObjectWith[keyToBeTranslated];
                }
            });
            if (Object.keys(translationToDo).length > 0) {
                translationsToDo.push({ to: translation.locale, data: translationToDo });
            }
        });
        if (translationsToDo.length > 0) {
            let translationResponse = await $axios.$post("/v1/google-translations/translate", {
                translationsToDo: translationsToDo
            });
            translations = translations.map(translation => {
                if (translationResponse[translation.locale]) {
                    translation = { ...translation, ...translationResponse[translation.locale] };
                }
                return translation
            });
        }
        return translations;
    });
}