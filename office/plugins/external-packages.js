import Vue from 'vue';
import VueQuillEditor from 'vue-quill-editor';
import VuetifyConfirm from 'vuetify-confirm';

Vue.use(VueQuillEditor);

export default ({ app }) => {
  Vue.use(VuetifyConfirm, { vuetify: app.vuetify })
}
