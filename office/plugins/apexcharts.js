import Vue from "vue";
import ApexCharts from 'vue-apexcharts'

//Using as vue components no need to import
Vue.use(ApexCharts)
Vue.component('ApexCharts', {
    extends: ApexCharts
})