const Menu = [
    { header: 'Application Information' },
    {
        title: 'Dashboard',
        group: 'apps',
        icon: 'mdi-monitor-dashboard',
        name: 'dashboard',
        route: 'index'
    },
    { header: 'User Details' },
    {
        title: 'Users',
        group: 'users',
        icon: 'mdi-account-group',
        items: [
            {
                title: 'My Account',
                group: 'todoers',
                items: [
                    { name: 'todoer-list', title: 'Profile', route: 'users-todoers', icon: 'mdi-account-details' },
                ]
            },
        ]
    },
    { header: 'Activity Details' },
    {
        title: 'Activities',
        group: 'activities',
        icon: 'mdi-account-group',
        items: [
            {
                title: 'Todos',
                group: 'todos',
                items: [
                    { name: 'todo-list', title: 'View', route: 'activities-todos', icon: 'mdi-home-clock' },
                    { name: 'todo-create', title: 'Create', route: 'activities-todos-create', icon: 'mdi-clock-plus' },
                ]
            },
        ]
    },
    // { divider: true },
    
];
// reorder menu alphabetically
// Menu.forEach((item) => {
//     if (item.items) {
//         item.items.sort((x, y) => {
//             let textA = x.title.toUpperCase();
//             let textB = y.title.toUpperCase();
//             return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
//         });
//     }
// });

export default Menu;