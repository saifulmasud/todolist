const Menu = [
    { header: 'Application Information' },
    {
        title: 'Dashboard',
        group: 'apps',
        icon: 'mdi-monitor-dashboard',
        name: 'dashboard',
        route: 'index'
    },
    { header: 'User Details' },
    {
        title: 'Users',
        group: 'users',
        icon: 'mdi-account-group',
        items: [
            {
                title: 'Todoers',
                group: 'todoers',
                items: [
                    { name: 'todoer-list', title: 'View', route: 'users-todoers', icon: 'mdi-account-details' },
                    { name: 'todoer-create', title: 'Create', route: 'users-todoers-create', icon: 'mdi-account-plus' },
                ]
            },
            {
                title: 'Admins',
                group: 'admins',
                items: [
                    { name: 'admin-list', title: 'Profile', route: 'users-admins', icon: 'mdi-account-details' },
                ]
            },
        ]
    },
    { header: 'Activity Details' },
    {
        title: 'Activities',
        group: 'activities',
        icon: 'mdi-account-group',
        items: [
            {
                title: 'Todos',
                group: 'todos',
                items: [
                    { name: 'todo-list', title: 'View', route: 'activities-todos', icon: 'mdi-home-clock' },
                    { name: 'todo-create', title: 'Create', route: 'activities-todos-create', icon: 'mdi-clock-plus' },
                ]
            },
        ]
    },
    { header: 'Configurations' },
    {
        title: 'Settings',
        group: 'settings',
        icon: 'mdi-cog-outline',
        items: [
            {
                title: 'Manage Todoers',
                group: 'permissions',
                items: [
                    { name: 'permission-todoer', title: 'Todoer Permission', route: 'settings-permissions-todoers', icon: 'mdi-account-lock-open-outline' },
                ]
            },
        ]
    },
    // { divider: true },
    
];
// reorder menu alphabetically
// Menu.forEach((item) => {
//     if (item.items) {
//         item.items.sort((x, y) => {
//             let textA = x.title.toUpperCase();
//             let textB = y.title.toUpperCase();
//             return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
//         });
//     }
// });

export default Menu;
