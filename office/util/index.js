
export function camel(str) {
    const camel = (str || '').replace(/-([^-])/g, g => g[1].toUpperCase());

    return capitalize(camel);
}

export function camelActual(str) {
    return (str || '').replace(/-(\w)/g, (_, c) => (c ? c.toUpperCase() : ''));
}

export function kebab(str) {
    return (str || '').replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase();
}

export function capitalize(str) {
    str = str || '';
    return `${str.substr(0, 1).toUpperCase()}${str.slice(1)}`;
}

export function findProduct(store, id) {
    return store.state.store.products.find(p => p.id === id);
}

export function isOnSale(variants) {
    return variants.some(variant => {
        return parseFloat(variant.price) < parseFloat(variant.compareAtPrice);
    });
}

export function randomNumber(min, max) {
    return Math.floor(Math.random() * max) + min;
}

export function randomString(length = 5) {
    let text = '';
    const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    for (let i = 0; i < length; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;
}
export function randomElement(arr = []) {
    return arr[Math.floor(Math.random() * arr.length)];
};

export function randomByRange(min, max){
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min; //The maximum is inclusive and the minimum is inclusive
};

export function paginationData(data){
    return {
        links: {
            first: data.first_page_url,
            last: data.last_page_url,
            prev: data.prev_page_url,
            next: data.next_page_url
        },
        meta: {
            current_page: data.current_page,
            from: data.from,
            last_page: data.last_page,
            path: data.path,
            per_page: data.per_page,
            to: data.to,
            total: data.total
        }
    }
};

export function toggleFullScreen(){
    let doc = window.document;
    let docEl = doc.documentElement;

    let requestFullScreen = docEl.requestFullscreen || docEl.mozRequestFullScreen || docEl.webkitRequestFullScreen || docEl.msRequestFullscreen;
    let cancelFullScreen = doc.exitFullscreen || doc.mozCancelFullScreen || doc.webkitExitFullscreen || doc.msExitFullscreen;

    if (!doc.fullscreenElement && !doc.mozFullScreenElement && !doc.webkitFullscreenElement && !doc.msFullscreenElement) {
        requestFullScreen.call(docEl);
    }
    else {
        cancelFullScreen.call(doc);
    }
}

export function makeAlias(id, name){
    let kebabCaseName = name.trim().replace(/([a-z])([A-Z])/g, '$1-$2').replace(/[\s_]+/g, '-').toLowerCase();
    return id + '-' + kebabCaseName;
}

export function getIdFromAlias(alias){
    return alias.split('-')[0];
};

export default {
};