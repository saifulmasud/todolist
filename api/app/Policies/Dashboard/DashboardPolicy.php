<?php

namespace App\Policies\Dashboard;

use App\Models\User\User;
use App\Models\Dashboard\Dashboard;
use Illuminate\Auth\Access\HandlesAuthorization;

class DashboardPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function viewAny(User $user)
    {
        if($user->id == 1) {
            return true;
        }else {
            if(($user->id != null) && 
                ($user->role != null) && 
                (request()->user->id == $user->id)) {
                return true;
            }else {
                return false;
            }
        }
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User\User  $user
     * @param  \App\Models\Dashboard $dashboard
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(User $user, Dashboard $dashboard)
    {
        //
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(User $user)
    {
        if($user->id == 1) {
            return true;
        }else {
            if(($user->id != null) && 
                ($user->role != null) && 
                (request()->user()->id == $user->id)) {
                return true;
            }else {
                return false;
            }
        }
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User\User  $user
     * @param  \App\Models\Dashboard $dashboard
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(User $user, Dashboard $dashboard)
    {

    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User\User  $user
     * @param  \App\Models\Dashboard $dashboard
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(User $user, Dashboard $dashboard)
    {

    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User\User  $user
     * @param  \App\Models\Dashboard $dashboard
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function restore(User $user, Dashboard $dashboard)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User\User  $user
     * @param  \App\Models\Dashboard $dashboard
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function forceDelete(User $user, Dashboard $dashboard)
    {
        //
    }
}
