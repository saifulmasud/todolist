<?php

namespace App\Policies\User;

use App\Models\User\User;
use App\Models\Setting\Permission;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function viewAny(User $user)
    {
        $requestForRole = request()->route()->parameter('role');
        if($user->id == 1) {
            return true;
        }else if(($user->role == $requestForRole) && 
            ($user->id == request()->user()->id) &&
            ($user->permissions()->where('permission_name_id', 1)->where('permission_action_id', 1)->count() > 0)) {
            return true;
        }else {
            return false;
        }
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User\User  $user
     * @param  \App\Models\User\User  $model
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(User $user, User $model)
    {

    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(User $user)
    {
        $requestForRole = request()->route()->parameter('role');
        if($user->id == 1) {
            return true;
        }else if(($user->role == $requestForRole) && 
            ($user->id == request()->user()->id)) {
            return false;
        }else {
            return false;  
        }
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User\User  $user
     * @param  \App\Models\User\User  $model
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(User $user, User $model)
    {
        $requestForRole = request()->route()->parameter('role');
        if($user->id == 1) {
            return true;
        }else if (($user->id == $model->id) && 
            ($user->role == $model->role) &&
            ($user->permissions()->where('permission_name_id', 1)->where('permission_action_id', 3)->count() > 0)) {
            return true;
        }else {
            return false;
        }
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User\User  $user
     * @param  \App\Models\User\User  $model
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(User $user, User $model)
    {
        if($user->id == 1) {
            return true;
        }else { 
            return false;
        }
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User\User  $user
     * @param  \App\Models\User\User  $model
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function restore(User $user, User $model)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User\User  $user
     * @param  \App\Models\User\User  $model
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function forceDelete(User $user, User $model)
    {
        //
    }
}
