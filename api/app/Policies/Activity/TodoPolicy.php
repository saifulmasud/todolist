<?php

namespace App\Policies\Activity;

use App\Models\User\User;
use App\Models\Activity\Todo;
use Illuminate\Auth\Access\HandlesAuthorization;

class TodoPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function viewAny(User $user)
    {
        if($user->id == 1) {
            return true;
        }else if($user->permissions()->where('permission_name_id', 2)->where('permission_action_id', 1)->count() > 0) {
            return true;
        }else {
            return false;
        }
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User\User  $user
     * @param  \App\Models\Activity\Todo $todo
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(User $user, Todo $todo)
    {
        //
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(User $user)
    {
        if($user->id == 1) {
            return true;
        }else if($user->permissions()->where('permission_name_id', 2)->where('permission_action_id', 2)->count() > 0) {
            return true;
        }else {
            return false;
        }
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User\User  $user
     * @param  \App\Models\Activity\Todo $todo
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(User $user, Todo $todo)
    {
        $todoer = $todo->todoer;
        if($user->id == 1) {
            return true;
        }else if($user->permissions()->where('permission_name_id', 2)->where('permission_action_id', 3)->count() > 0) {
            if($user->id == $todoer->id) {
                return true;
            }else {
                return false;
            }
        }else {
            return false;
        }
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User\User  $user
     * @param  \App\Models\Activity\Todo $todo
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(User $user, Todo $todo)
    {
        $todoer = $todo->todoer;
        if($user->id == 1) {
            return true;
        }else if($user->permissions()->where('permission_name_id', 2)->where('permission_action_id', 4)->count() > 0) {
            if($user->id == $todoer->id) {
                return true;
            }else {
                return false;
            }
        }else {
            return false;
        }
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User\User  $user
     * @param  \App\Models\TeleConsult\Schedule $schedule
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function restore(User $user, Schedule $schedule)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User\User  $user
     * @param  \App\Models\TeleConsult\Schedule $schedule
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function forceDelete(User $user, Schedule $schedule)
    {
        //
    }
}
