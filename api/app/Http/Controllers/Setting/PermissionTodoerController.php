<?php

namespace App\Http\Controllers\Setting;

use DB;
use App\Models\User\User;
use App\Models\User\UserInfo;
use App\Http\Controllers\Controller;
use App\Http\Requests\Setting\PermissionRequest;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class PermissionTodoerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = User::query()->with('info', 'permissions');
        $users->whereRole('todoer');

        $searchKey = $request->input('search');
        if (!empty($searchKey)) {
            $users->whereRaw("CONCAT_WS(' ', name, surname) like '%" . $searchKey . "%' ")
            ->whereRole('todoer');

            $users->orWhere('email', 'LIKE', "%{$searchKey}%")
            ->whereRole('todoer');

            $users->orWhere('phone', 'LIKE', "%{$searchKey}%")
            ->whereRole('todoer');

            $users->orWhere('mobile', 'LIKE', "%{$searchKey}%")
            ->whereRole('todoer');

            $users->orWhere('role', 'LIKE', "%{$searchKey}%")
            ->whereRole('todoer');

            $users->orWhere(function (Builder $query) use ($searchKey) {
                $query->whereHas('permissions', function (Builder $query) use ($searchKey) {
                    if(($searchKey == 'Profile') || 
                        ($searchKey == 'profile')) {
                        $query->where('permission_name_id', 1);
                    }else if(($searchKey == 'Todos') || 
                        ($searchKey == 'todos') ||
                        ($searchKey == 'Todo') ||
                        ($searchKey == 'todo')) {
                        $query->where('permission_name_id', 2);
                    }else{
                        $query->where('permission_description', 'LIKE', "%{$searchKey}%");
                    }
                });
            })->whereRole('todoer');

            $users->orWhere(function ($query) use ($searchKey) {
                $query->whereHas('permissions', function (Builder $query) use ($searchKey) {
                    $query->where('permission_description', 'LIKE', "%{$searchKey}%"); 
                });
            })->whereRole('todoer');

            $users->orWhere(function (Builder $query) use ($searchKey) {
                if($searchKey == 'enable' || $searchKey == 'Enable') {
                    $query->where('status', 1)
                    ->whereRole('todoer');
                }
                if($searchKey == 'disable' || $searchKey == 'Disable') {
                    $query->where('status', 0)
                    ->whereRole('todoer');
                }
            })->whereRole('todoer');
        }

        $usersCount = $users->count();
        if ($request->input('itemsPerPage') > 0) {
            $users->limit($request->input('itemsPerPage'))->skip($request->input('itemsPerPage') * ($request->input('page') - 1));
        }

        $sortByKeys = collect($request->input('sortBy'));
        if ($sortByKeys->isNotEmpty()) {
            $sortByMatrix = $sortByKeys->crossJoin($request->input('sortDesc'));
            $sortBy = $sortByMatrix->map(function ($item, $key) {
                return [
                    $item[0], filter_var($item[1], FILTER_VALIDATE_BOOLEAN) ? 'DESC' : 'ASC'
                ];
            });
            $sortBy->eachSpread(function($sortCol, $sortType) use($users, $request){
                if($sortCol == 'full_name') {
                    $users->orderByRaw("CONCAT_WS(' ', name, surname) $sortType")
                    ->whereRole('todoer');
                }else if($sortCol == 'email') {
                    $users->orderByRaw("email $sortType")
                    ->whereRole('todoer');
                }else if($sortCol == 'phone') {
                    $users->orderByRaw("phone $sortType")
                    ->whereRole('todoer');
                }else if($sortCol == 'mobile') {
                    $users->orderByRaw("mobile $sortType")
                    ->whereRole('todoer');
                }else if($sortCol == 'role') {
                    $users->orderByRaw("role $sortType")
                    ->whereRole('todoer');
                }else if($sortCol == 'permission_description') {
                    $users
                    ->join('permissions', 'users.id', '=', 'permissions.user_id')
                    ->orderByRaw("permissions.permission_description $sortType")
                    ->select('users.*');
                }else if($sortCol == 'status') {
                    $users->orderByRaw("status $sortType")
                    ->whereRole('todoer');
                }else{
                    $users->orderBy($sortCol, $sortType)
                    ->whereRole('todoer');
                }
            });
        }else{
            $users->latest()->whereRole('todoer');
        }

        $users = $users->get()->toArray();
        $data = [];
        $data['data'] = $users;
        $data['count'] = $usersCount;

        return $this->respond($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PermissionRequest $request)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        try {
            $user = User::whereId($id)->firstOrFail();
            $user->load('permissions');

            $data = [
                'user' => $user,
            ];
            return $this->respond($data);

        } catch (\Throwable $th) {
            return $this->respondNotFound($th->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PermissionRequest $request, $id)
    {
        $this->authorize('update', User::find($id));

        DB::beginTransaction();
        try {
            $userIds = collect($request->input('user_id'));
            $userIds = $userIds->pluck('id');
            $permissionNames = $request->input('permission_name');
            $permissionActions = $request->input('permission_action');

            $selectedUsers = User::whereIn('id', $userIds)->get();

            foreach($selectedUsers as $user) {
                $permissionData = $user->permissions()
                ->whereIn('permission_name_id', $permissionNames)
                ->whereIn('permission_action_id', $permissionActions);
                $permissionData->forceDelete();

                foreach($permissionNames as $name) {
                    foreach($permissionActions as $action) {
                        $user->permissions()->create([
                            'user_id' => $user->id,
                            'permission_name_id' => $name, 
                            'permission_action_id' => $action, 
                            'permission_description' => $request->permission_description,
                            'created_by' => $request->created_by,
                        ]);
                    }
                }
            }

            DB::commit();

            return $this->respondUpdated('Permission updated successfully.', [
                'selectedUsers' => $selectedUsers,
            ]);
        } catch (ModelNotFoundException $e) {
            DB::rollback();
            return $this->respondWithExpectationFailed('Sorry, Operation Failed');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $userIds = $request->input('user_id');
            $permissionNames = $request->input('permission_name');
            $permissionActions = $request->input('permission_action');

            $selectedUsers = User::whereIn('id', $userIds)->get();

            foreach($selectedUsers as $user) {
                $permissionData = $user->permissions()
                ->whereIn('permission_name_id', $permissionNames)
                ->whereIn('permission_action_id', $permissionActions);
                $permissionData->forceDelete();
            }
            
            DB::commit();

            return $this->respond([
                'status' => 1,
                'message' => 'Succefully Deleted'
            ]);
        } catch (ModelNotFoundException $e) {
            return $this->respondNotFound('Sorry, Operation Failed');
        }
    }
}
