<?php

namespace App\Http\Controllers\Activity;

use DB;
use App\Models\User\User;
use App\Models\Activity\Todo;
use App\Models\Activity\TodoInfo;
use App\Http\Requests\Activity\TodoRequest;
use App\Classes\TeleConsult\ScheduleChunkTimeGenerator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class TodoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('viewAny', Todo::class);

        if($request->user()->id ==1) {
            $todos = Todo::query()->with([
                'info', 
                'todoer', 
                'todoer.info',
            ]);

            $searchKey = $request->input('search');
            if (!empty($searchKey)) {
                $todos->where(function (Builder $query) use ($searchKey) {
                    $query->whereHas('todoer', function (Builder $query) use($searchKey){
                        $query->whereRaw("CONCAT_WS(' ', name, surname) like '%" . $searchKey . "%' ");
                    });
                });

                $todos->orWhere('title', 'LIKE', "%{$searchKey}%");

                $todos->orWhere('min_date', 'LIKE', "%{$searchKey}%");

                $todos->orWhere('start_time', 'LIKE', "%{$searchKey}%");

                $todos->orWhere('end_time', 'LIKE', "%{$searchKey}%");

                $todos->orWhere(function (Builder $query) use ($searchKey) {
                    if($searchKey === 'enable' || $searchKey === 'Enable') {
                        $query->where('status', 1);
                    }
                    if($searchKey == 'disable' || $searchKey == 'Disable') {
                        $query->where('status', 0);
                    }
                });
            }

            $todoCount = $todos->count();

            if ($request->input('itemsPerPage') > 0) {
                $todos->limit($request->input('itemsPerPage'))->skip($request->input('itemsPerPage') * ($request->input('page') - 1));
            }

            $sortByKeys = collect($request->input('sortBy'));
            if ($sortByKeys->isNotEmpty()) {
                $sortByMatrix = $sortByKeys->crossJoin($request->input('sortDesc'));
                $sortBy = $sortByMatrix->map(function ($item, $key) {
                    return [
                        $item[0], filter_var($item[1], FILTER_VALIDATE_BOOLEAN) ? 'DESC' : 'ASC'
                    ];
                });

                $sortBy->eachSpread(function($sortCol, $sortType) use($todos) {
                    if($sortCol == 'todoer_id'){
                        $todos
                        ->join('users', 'todos.todoer_id', '=', 'users.id')
                        ->orderByRaw("CONCAT_WS(' ', users.name, users.surname) $sortType")
                        ->select('todos.*');                        
                    }else if($sortCol == 'title') {
                        $todos->orderByRaw("title $sortType");
                    }else if($sortCol == 'min_date') {
                        $todos->orderByRaw("min_date $sortType");
                    }else if($sortCol == 'start_time') {
                        $todos->orderByRaw("start_time $sortType");
                    }else if($sortCol == 'end_time') {
                        $todos->orderByRaw("end_time $sortType");
                    }else if($sortCol == 'status') {
                        $todos->orderByRaw("status $sortType");
                    }else{
                        $todos->orderBy($sortCol, $sortType);
                    }
                });
            }else{
                $todos->latest();
            }

            $todos = $todos->get()->toArray();
            $data = [];
            $data['data'] = $todos;
            $data['count'] = $todoCount;
    
            return $this->respond($data);            
        }else if($request->user()->role == 'todoer') {
            $todos = Todo::query()->with([
                'info', 
                'todoer', 
                'todoer.info',
            ])->where('todoer_id', $request->user()->id);

            $searchKey = $request->input('search');
            if (!empty($searchKey)) {
                $todos->where(function (Builder $query) use ($searchKey) {
                    $query->whereHas('todoer', function (Builder $query) use($searchKey){
                        $query->whereRaw("CONCAT_WS(' ', name, surname) like '%" . $searchKey . "%' ");
                    });
                });

                $todos->orWhere('title', 'LIKE', "%{$searchKey}%")
                ->where('todoer_id', $request->user()->id);

                $todos->orWhere('min_date', 'LIKE', "%{$searchKey}%")
                ->where('todoer_id', $request->user()->id);

                $todos->orWhere('start_time', 'LIKE', "%{$searchKey}%")
                ->where('todoer_id', $request->user()->id);

                $todos->orWhere('end_time', 'LIKE', "%{$searchKey}%")
                ->where('todoer_id', $request->user()->id);

                $todos->orWhere(function (Builder $query) use ($searchKey) {
                    if($searchKey === 'enable' || $searchKey === 'Enable') {
                        $query->where('status', 1);
                    }
                    if($searchKey == 'disable' || $searchKey == 'Disable') {
                        $query->where('status', 0);
                    }
                })->where('todoer_id', $request->user()->id);
            }

            $todoCount = $todos->where('todoer_id', $request->user()->id)->count();

            if ($request->input('itemsPerPage') > 0) {
                $todos->limit($request->input('itemsPerPage'))->skip($request->input('itemsPerPage') * ($request->input('page') - 1));
            }

            $sortByKeys = collect($request->input('sortBy'));
            if ($sortByKeys->isNotEmpty()) {
                $sortByMatrix = $sortByKeys->crossJoin($request->input('sortDesc'));
                $sortBy = $sortByMatrix->map(function ($item, $key) {
                    return [
                        $item[0], filter_var($item[1], FILTER_VALIDATE_BOOLEAN) ? 'DESC' : 'ASC'
                    ];
                });

                $sortBy->eachSpread(function($sortCol, $sortType) use($todos, $request) {
                    if($sortCol == 'todoer_id'){
                        $todos
                        ->where('todoer_id', $request->user()->id)
                        ->join('users', 'todos.todoer_id', '=', 'users.id')
                        ->orderByRaw("CONCAT_WS(' ', users.name, users.surname) $sortType")
                        ->select('todos.*');                        
                    }else if($sortCol == 'title') {
                        $todos->orderByRaw("title $sortType")
                        ->where('todoer_id', $request->user()->id);
                    }else if($sortCol == 'min_date') {
                        $todos->orderByRaw("min_date $sortType")
                        ->where('todoer_id', $request->user()->id);
                    }else if($sortCol == 'start_time') {
                        $todos->orderByRaw("start_time $sortType")
                        ->where('todoer_id', $request->user()->id);
                    }else if($sortCol == 'end_time') {
                        $todos->orderByRaw("end_time $sortType")
                        ->where('todoer_id', $request->user()->id);
                    }else if($sortCol == 'status') {
                        $todos->orderByRaw("status $sortType")
                        ->where('todoer_id', $request->user()->id);
                    }else{
                        $todos->orderBy($sortCol, $sortType)
                        ->where('todoer_id', $request->user()->id);
                    }
                });
            }else{
                $todos
                ->where('todoer_id', $request->user()->id)
                ->latest();
            }

            $todos = $todos->get()->toArray();
            $data = [];
            $data['data'] = $todos;
            $data['count'] = $todoCount;
    
            return $this->respond($data);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', Todo::class);

        $chunkTimeGen = new ScheduleChunkTimeGenerator();
        $currDate = $chunkTimeGen->getCurrDate();
        $nextDateOfCurrDate = $chunkTimeGen->getNextDateOfAnyDate($currDate);

        if($request->user()->id == 1) {
            $todoers = User::with('info')
            ->whereRole('todoer')
            ->get();

            $data = [
                'todoers' => $todoers,
                'nextDateOfCurrDate' => $nextDateOfCurrDate,
            ];
            return $this->respond($data);

        }else if($request->user()->role == 'todoer') {
            $todoer = User::findOrFail($request->user()->id);

            $data = [
                'todoers' => [$todoer],
                'nextDateOfCurrDate' => $nextDateOfCurrDate,
            ];
            return $this->respond($data); 
        }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TodoRequest $request)
    {
        //$this->authorize('create', Todo::class);

        DB::beginTransaction();
        try {
            //Insert data into todo table
            $todo = new Todo();
            $todoFillableData = $request->only($todo->getFillable());
            $todo->fill($todoFillableData)->save();

            //Insert data into todo_infos table
            $todoInfo = new TodoInfo();
            $todoInfoFillableData = collect($request->input('info'))->only($todoInfo->getFillable());
            $todoInfoTranslationsData = collect($request->input('info.translations'))->keyBy('locale');
            $todoInfoData = $todoInfoTranslationsData->merge($todoInfoFillableData)->all();
            
            $todoInfo->fill($todoInfoData);
            $todoInfo->todo()->associate($todo);
            $todoInfo->save();

            DB::commit();

            return $this->respondCreated('Todo created successfully.', [
                'todo' => $todo,
            ]);
        } catch (ModelNotFoundException $e) {
            DB::rollback();
            return $this->respondWithExpectationFailed('Sorry, Operation Failed');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $this->authorize('update', Todo::find($id));

        try {
            $todo = Todo::whereId($id)->firstOrFail();

            $todo->load(
                'info', 
                'info.translations',
                'todoer',
                'todoer.info',
            );

            $data = [
                'todo' => $todo,
            ];
            return $this->respond($data);

        } catch (\Throwable $th) {
            return $this->respondNotFound($th->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TodoRequest $request, $id)
    {
        $this->authorize('update', Todo::find($id));

        DB::beginTransaction();
        try {
            $todo = Todo::whereId($id)->firstOrFail();
            $todoFillableData = $request->only($todo->getFillable());
            $todo->fill($todoFillableData)->save();

            $todoInfo = $todo->info;
            $todoInfoFillableData = collect($request->input('info'))->only($todoInfo->getFillable());
            $todoInfoTranslationsData = collect($request->input('info.translations'))->keyBy('locale');
            $todoInfoData = $todoInfoTranslationsData->merge($todoInfoFillableData)->all();
            
            $todoInfo->fill($todoInfoData);
            $todoInfo->save();

            DB::commit();

            return $this->respondUpdated('Todo updated successfully.', [
                'todo' => $todo,
            ]);
        } catch (ModelNotFoundException $e) {
            DB::rollback();
            return $this->respondWithExpectationFailed('Sorry, Operation Failed');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $this->authorize('delete', Todo::find($id));

        DB::beginTransaction();
        try {
            $todo = Todo::whereId($id)->firstOrFail();
            
            if(($request->user()->id ==1) || 
                (($request->user()->role == 'todoer') && 
                (User::find($request->user()->id)->permissions()->where('permission_name_id', 2)->where('permission_action_id', 4)->count() > 0) &&
                ($todo->todoer_id == $request->user()->id))) {
                if((!$todo->info()->exists() || 
                    ($todo->info()->exists() && $todo->info()->delete())) && 
                    $todo->delete()) {
                    DB::commit();
                    return $this->respond([
                        'status' => 1,
                        'message' => 'Succefully Deleted'
                    ]);
                } else {
                    DB::rollback();
                    return $this->respond([
                        'status' => 0,
                        'message' => 'Deleting Failed.'
                    ]);
                }
            }
        } catch (ModelNotFoundException $e) {
            return $this->respondNotFound('Sorry, Operation Failed');
        }
    }
}
