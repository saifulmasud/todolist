<?php

namespace App\Http\Controllers\Auth;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use Illuminate\Support\Facades\Auth;

use App\Models\User\User;

class AuthController extends Controller
{

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login']]);
    }

    public function login(LoginRequest $request)
    {
        if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')])) {
            $user = Auth::user();
            if(Auth::user()->role !== 'official') {
                $token =  $user->createToken($user->email)->plainTextToken;
                return $this->respondWithToken($token, $user, null);
            }else {
                return $this->respondWithUnauthorisedError('User ID or Password mismatch');
            }
        } else {
            return $this->respondWithUnauthorisedError('User ID or Password mismatch');
        }
    }

    public function me(Request $request)
    {
        $user = Auth::user();
        if($user->role !== 'official') {
            $user->load('info', 'permissions');
            return $this->respond([
                'status' => 1,
                'user' => $user
            ]);
        }
    }

    public function logout()
    {
        Auth::user()->currentAccessToken()->delete();
        return $this->respond(['message' => 'Successfully Logged Out.']);
    }

    public function logoutUserPermissionsSet(Request $request, $id)
    {
        if($request->input('user_id')) {
            $userIds = collect($request->input('user_id'));
            $userIds = $userIds->pluck('id');
            $selectedUsers = User::whereIn('id', $userIds)->get();
    
            foreach($selectedUsers as $user) {
                $user->tokens()->delete();
            }
    
            return $this->respond(['message' => 'Successfully Logged Out.']);
        }
    }
}
