<?php

namespace App\Http\Controllers\Signingup;

use DB;
use App\Models\User\User;
use App\Models\User\UserInfo;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\UserRequest;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class SigningupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $role)
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clientUsers = User::with('info')->whereRole('client')->orWhere('id',1)->get();
        return $this->respond([
            'clientUsers' => $clientUsers,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        DB::beginTransaction();
        try {
            //Insert data into users table
            $user = new User();
            $userFillableData = $request->only($user->getFillable());
            $user->fill($userFillableData)->save();
            $user->save();

            //Insert data into user_infos table
            $userInfo = new UserInfo();
            $userInfoFillableData = collect($request->input('info'))->only($userInfo->getFillable());
            $userInfoTranslationsData = collect($request->input('info.translations'))->keyBy('locale');
            $userInfoData = $userInfoTranslationsData->merge($userInfoFillableData)->all();
            
            $userInfo->fill($userInfoData);
            $userInfo->user()->associate($user);
            $userInfo->save();

            $user->update(['created_by' => $user->id]);

            DB::commit();

            return $this->respondCreated('User created successfully.', [
                'user' => $user,
            ]);
        } catch (ModelNotFoundException $e) {
            DB::rollback();
            return $this->respondWithExpectationFailed('Sorry, Operation Failed');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $role, $id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $role, $id)
    {
        DB::beginTransaction();
        try {
            $signingupDatas = collect($request->input('id'));
            $signingUpIds = $signingupDatas->pluck('id');

            foreach($signingUpIds as $signingUpId) {
                $user = User::whereId($signingUpId)->whereRole($role)->firstOrFail();
                $user->update([
                    'role' => $request->role,
                    'created_by' => $request->created_by,
                ]);
            }

            DB::commit();

            return $this->respondUpdated('User updated successfully.', [
                'user' => $user,
            ]);
        } catch (ModelNotFoundException $e) {
            DB::rollback();
            return $this->respondWithExpectationFailed('Sorry, Operation Failed');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $role, $id)
    {

    }
}
