<?php

namespace App\Http\Controllers\User;

use DB;
use App\Models\User\User;
use App\Models\User\UserInfo;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\UserRequest;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $role)
    {
        $this->authorize('viewAny', User::class);

        $users = User::query()->with('info', 'permissions');
        $users->whereRole($role);

        if($request->user()->id == 1) {
            $searchKey = $request->input('search');
            if (!empty($searchKey)) {
                $users->whereRaw("CONCAT_WS(' ', name, surname) like '%" . $searchKey . "%' ")
                ->whereRole($role);

                $users->orWhere('email', 'LIKE', "%{$searchKey}%")
                ->whereRole($role);

                $users->orWhere('mobile', 'LIKE', "%{$searchKey}%")
                ->whereRole($role);

                $users->orWhere(function (Builder $query) use ($searchKey, $request) {
                    if($searchKey === 'enable' || $searchKey === 'Enable') {
                        $query->where('status', 1);
                    }
                    if($searchKey == 'disable' || $searchKey == 'Disable') {
                        $query->where('status', 0);
                    }
                })->whereRole($role);
            }

            $usersCount = $users->count();
            if ($request->input('itemsPerPage') > 0) {
                $users->limit($request->input('itemsPerPage'))
                ->skip($request->input('itemsPerPage') * ($request->input('page') - 1));
            }

            $sortByKeys = collect($request->input('sortBy'));
            if ($sortByKeys->isNotEmpty()) {
                $sortByMatrix = $sortByKeys->crossJoin($request->input('sortDesc'));
                $sortBy = $sortByMatrix->map(function ($item, $key) {
                    return [
                        $item[0], filter_var($item[1], FILTER_VALIDATE_BOOLEAN) ? 'DESC' : 'ASC'
                    ];
                });
                $sortBy->eachSpread(function($sortCol, $sortType) use($users, $role){
                    if($sortCol == 'full_name'){
                        $users->orderByRaw("CONCAT_WS(' ', name, surname) $sortType")
                        ->whereRole($role);

                    }else if($sortCol == 'email') {
                        $users->orderByRaw("email $sortType")
                        ->whereRole($role);

                    }else if($sortCol == 'mobile') {
                        $users->orderByRaw("mobile $sortType")
                        ->whereRole($role);

                    }else if($sortCol == 'status') {
                        $users->orderByRaw("status $sortType")
                        ->whereRole($role);

                    }else{
                        $users->orderBy($sortCol, $sortType)
                        ->whereRole($role);
                    }
                });
            }else{
                $users->whereRole($role)->latest();
            }
            
            $users = $users->get()->toArray();
            $data = [];
            $data['data'] = $users;
            $data['count'] = $usersCount;
    
            return $this->respond($data);
        }else if($request->user()->role == $role) {
            $users->whereId($request->user()->id);

            $searchKey = $request->input('search');
            if (!empty($searchKey)) {
                $users->whereRaw("CONCAT_WS(' ', name, surname) like '%" . $searchKey . "%' ")
                ->whereRole($role);

                $users->orWhere('email', 'LIKE', "%{$searchKey}%")
                ->whereRole($role);

                $users->orWhere('mobile', 'LIKE', "%{$searchKey}%")
                ->whereRole($role);

                $users->orWhere(function (Builder $query) use ($searchKey, $request) {
                    if($searchKey === 'enable' || $searchKey === 'Enable') {
                        $query->where('status', 1);
                    }
                    if($searchKey == 'disable' || $searchKey == 'Disable') {
                        $query->where('status', 0);
                    }
                })->whereRole($role);
            }

            $usersCount = $users->count();
            if ($request->input('itemsPerPage') > 0) {
                $users->limit($request->input('itemsPerPage'))
                ->skip($request->input('itemsPerPage') * ($request->input('page') - 1));
            }

            $sortByKeys = collect($request->input('sortBy'));
            if ($sortByKeys->isNotEmpty()) {
                $sortByMatrix = $sortByKeys->crossJoin($request->input('sortDesc'));
                $sortBy = $sortByMatrix->map(function ($item, $key) {
                    return [
                        $item[0], filter_var($item[1], FILTER_VALIDATE_BOOLEAN) ? 'DESC' : 'ASC'
                    ];
                });
                $sortBy->eachSpread(function($sortCol, $sortType) use($users, $role){
                    if($sortCol == 'full_name'){
                        $users->orderByRaw("CONCAT_WS(' ', name, surname) $sortType")
                        ->whereRole($role);

                    }else if($sortCol == 'email') {
                        $users->orderByRaw("email $sortType")
                        ->whereRole($role);

                    }else if($sortCol == 'mobile') {
                        $users->orderByRaw("mobile $sortType")
                        ->whereRole($role);

                    }else if($sortCol == 'status') {
                        $users->orderByRaw("status $sortType")
                        ->whereRole($role);

                    }else{
                        $users->orderBy($sortCol, $sortType)
                        ->whereRole($role);
                    }
                });
            }else{
                $users->whereRole($role)->latest();
            }
            
            $users = $users->get()->toArray();
            $data = [];
            $data['data'] = $users;
            $data['count'] = $usersCount;
    
            return $this->respond($data);
        }


        // if($request->user()->id !== 1) {
        //     if($role == $request->user()->role) {
        //         if(($request->input('rolealias') === 'dscclient') && 
        //             ($role === 'client')) {
        //             $users->whereRole($role)
        //             ->where('client_id', $request->user()->id)
        //             ->where('client_name', '!=', NULL);

        //             $searchKey = $request->input('search');
        //             if (!empty($searchKey)) {
        //                 $users->whereRaw("CONCAT_WS(' ', name, surname) like '%" . $searchKey . "%' ")
        //                 ->whereRole($role)
        //                 ->where('client_id', $request->user()->id)
        //                 ->where('client_name', '!=', NULL);
    
        //                 $users->orWhere('email', 'LIKE', "%{$searchKey}%")
        //                 ->whereRole($role)
        //                 ->where('client_id', $request->user()->id)
        //                 ->where('client_name', '!=', NULL);
    
        //                 $users->orWhere(function (Builder $query) use ($searchKey, $request) {
        //                     $query->whereHas('info', function (Builder $query) use ($searchKey, $request) {
        //                         $query->where('company_name', 'LIKE', "%{$searchKey}%");
        //                     });
        //                 })->whereRole($role)
        //                 ->where('client_id', $request->user()->id)
        //                 ->where('client_name', '!=', NULL);
    
        //                 $users->orWhere('client_name', 'LIKE', "%{$searchKey}%")
        //                 ->whereRole($role)
        //                 ->where('client_id', $request->user()->id)
        //                 ->where('client_name', '!=', NULL);
    
        //                 $users->orWhere('role', 'LIKE', "%{$searchKey}%")
        //                 ->whereRole($role)
        //                 ->where('client_id', $request->user()->id)
        //                 ->where('client_name', '!=', NULL);
    
        //                 $users->orWhere(function (Builder $query) use ($searchKey, $request) {
        //                     if($searchKey === 'enable' || $searchKey === 'Enable') {
        //                         $query->where('status', 1);
        //                     }
        //                     if($searchKey == 'disable' || $searchKey == 'Disable') {
        //                         $query->where('status', 0);
        //                     }
        //                 })->whereRole($role)
        //                 ->where('client_id', $request->user()->id)
        //                 ->where('client_name', '!=', NULL);
        //             }
        
        //             $usersCount = $users->count();

        //             if ($request->input('itemsPerPage') > 0) {
        //                 $users->limit($request->input('itemsPerPage'))
        //                 ->skip($request->input('itemsPerPage') * ($request->input('page') - 1));
        //             }
        
        //             $sortByKeys = collect($request->input('sortBy'));
        //             if ($sortByKeys->isNotEmpty()) {
        //                 $sortByMatrix = $sortByKeys->crossJoin($request->input('sortDesc'));
        //                 $sortBy = $sortByMatrix->map(function ($item, $key) {
        //                     return [
        //                         $item[0], filter_var($item[1], FILTER_VALIDATE_BOOLEAN) ? 'DESC' : 'ASC'
        //                     ];
        //                 });
        //                 $sortBy->eachSpread(function($sortCol, $sortType) use($users, $request, $role){
        //                     if($sortCol == 'full_name'){
        //                         $users->orderByRaw("CONCAT_WS(' ', name, surname) $sortType")
        //                         ->whereRole($role)
        //                         ->where('client_id', $request->user()->id)
        //                         ->where('client_name', '!=', NULL);
    
        //                     }else if($sortCol == 'email') {
        //                         $users->orderByRaw("email $sortType")
        //                         ->whereRole($role)
        //                         ->where('client_id', $request->user()->id)
        //                         ->where('client_name', '!=', NULL);
    
        //                     }else if($sortCol == 'info.company_name') {
        //                         $users->whereHas('info', function (Builder $query) use ($sortType, $request) {
        //                             $query->orderByRaw("company_name $sortType");
        //                         })->whereRole($role)
        //                         ->where('client_id', $request->user()->id)
        //                         ->where('client_name', '!=', NULL);
    
        //                     }else if($sortCol == 'client_name') {
        //                         $users->orderByRaw("client_name $sortType")
        //                         ->whereRole($role)
        //                         ->where('client_id', $request->user()->id)
        //                         ->where('client_name', '!=', NULL);
    
        //                     }else if($sortCol == 'role') {
        //                         $users->orderByRaw("role $sortType")
        //                         ->whereRole($role)
        //                         ->where('client_id', $request->user()->id)
        //                         ->where('client_name', '!=', NULL);
    
        //                     }else if($sortCol == 'status') {
        //                         $users->orderByRaw("status $sortType")
        //                         ->whereRole($role)
        //                         ->where('client_id', $request->user()->id)
        //                         ->where('client_name', '!=', NULL);
    
        //                     }else{
        //                         $users->orderBy($sortCol, $sortType)
        //                         ->whereRole($role)
        //                         ->where('client_id', $request->user()->id)
        //                         ->where('client_name', '!=', NULL);
        //                     }
        //                 });
        //             }else{
        //                 $users->whereRole($role)
        //                 ->where('client_id', $request->user()->id)
        //                 ->where('client_name', '!=', NULL)
        //                 ->latest();
        //             }
        
        //             $users = $users->get()->toArray();
        //             $data = [];
        //             $data['data'] = $users;
        //             $data['count'] = $usersCount;
            
        //             return $this->respond($data);
        //         }else {
        //             $users->where('id', $request->user()->id);
        //             $searchKey = $request->input('search');
        //             if (!empty($searchKey)) {
        //                 $users->whereRaw("CONCAT_WS(' ', name, surname) like '%" . $searchKey . "%' ")
        //                 ->where('id', $request->user()->id);
    
        //                 $users->orWhere('email', 'LIKE', "%{$searchKey}%")
        //                 ->where('id', $request->user()->id);
    
        //                 $users->orWhere(function (Builder $query) use ($searchKey, $request) {
        //                     $query->whereHas('info', function (Builder $query) use ($searchKey, $request) {
        //                         $query->where('company_name', 'LIKE', "%{$searchKey}%");
        //                     });
        //                 })->where('id', $request->user()->id);
    
        //                 $users->orWhere('client_name', 'LIKE', "%{$searchKey}%")
        //                 ->where('id', $request->user()->id);
    
        //                 $users->orWhere('role', 'LIKE', "%{$searchKey}%")
        //                 ->where('id', $request->user()->id);
    
        //                 $users->orWhere(function (Builder $query) use ($searchKey, $request) {
        //                     if($searchKey === 'enable' || $searchKey === 'Enable') {
        //                         $query->where('status', 1);
        //                     }
        //                     if($searchKey == 'disable' || $searchKey == 'Disable') {
        //                         $query->where('status', 0);
        //                     }
        //                 })->where('id', $request->user()->id);
        //             }
        
        //             $usersCount = $users->count();
        //             if ($request->input('itemsPerPage') > 0) {
        //                 $users->limit($request->input('itemsPerPage'))
        //                 ->skip($request->input('itemsPerPage') * ($request->input('page') - 1));
        //             }
        
        //             $sortByKeys = collect($request->input('sortBy'));
        //             if ($sortByKeys->isNotEmpty()) {
        //                 $sortByMatrix = $sortByKeys->crossJoin($request->input('sortDesc'));
        //                 $sortBy = $sortByMatrix->map(function ($item, $key) {
        //                     return [
        //                         $item[0], filter_var($item[1], FILTER_VALIDATE_BOOLEAN) ? 'DESC' : 'ASC'
        //                     ];
        //                 });
        //                 $sortBy->eachSpread(function($sortCol, $sortType) use($users, $request){
        //                     if($sortCol == 'full_name'){
        //                         $users->orderByRaw("CONCAT_WS(' ', name, surname) $sortType")
        //                         ->where('id', $request->user()->id);
    
        //                     }else if($sortCol == 'email') {
        //                         $users->orderByRaw("email $sortType")
        //                         ->where('id', $request->user()->id);
    
        //                     }else if($sortCol == 'info.company_name') {
        //                         $users->whereHas('info', function (Builder $query) use ($sortType, $request) {
        //                             $query->orderByRaw("company_name $sortType");
        //                         })->where('id', $request->user()->id);
    
        //                     }else if($sortCol == 'client_name') {
        //                         $users->orderByRaw("client_name $sortType")
        //                         ->where('id', $request->user()->id);
    
        //                     }else if($sortCol == 'role') {
        //                         $users->orderByRaw("role $sortType")
        //                         ->where('id', $request->user()->id);
    
        //                     }else if($sortCol == 'status') {
        //                         $users->orderByRaw("status $sortType")
        //                         ->where('id', $request->user()->id);
    
        //                     }else{
        //                         $users->orderBy($sortCol, $sortType)
        //                         ->where('id', $request->user()->id);
        //                     }
        //                 });
        //             }else{
        //                 $users->where('id', $request->user()->id)->latest();
        //             }
        
        //             $users = $users->get()->toArray();
        //             $data = [];
        //             $data['data'] = $users;
        //             $data['count'] = $usersCount;
            
        //             return $this->respond($data);
        //         }
        //     }else {
        //         if($request->user()->role == 'client') {
        //             $users->where('client_id', $request->user()->id);
                    
        //             $searchKey = $request->input('search');
        //             if (!empty($searchKey)) {
        //                 $users->whereRaw("CONCAT_WS(' ', name, surname) like '%" . $searchKey . "%' ")
        //                 ->where('client_id', $request->user()->id)
        //                 ->whereRole($role);
    
        //                 $users->orWhere('email', 'LIKE', "%{$searchKey}%")
        //                 ->where('client_id', $request->user()->id)
        //                 ->whereRole($role);
    
        //                 $users->orWhere(function (Builder $query) use ($searchKey, $request) {
        //                     $query->whereHas('info', function (Builder $query) use ($searchKey, $request) {
        //                         $query->where('company_name', 'LIKE', "%{$searchKey}%");
        //                     });
        //                 })
        //                 ->where('client_id', $request->user()->id)
        //                 ->whereRole($role);
    
        //                 $users->orWhere('client_name', 'LIKE', "%{$searchKey}%")
        //                 ->where('client_id', $request->user()->id)
        //                 ->whereRole($role);
    
        //                 $users->orWhere('role', 'LIKE', "%{$searchKey}%")
        //                 ->where('client_id', $request->user()->id)
        //                 ->whereRole($role);
    
        //                 $users->orWhere(function (Builder $query) use ($searchKey, $request) {
        //                     if($searchKey === 'enable' || $searchKey === 'Enable') {
        //                         $query->where('status', 1);
        //                     }
        //                     if($searchKey == 'disable' || $searchKey == 'Disable') {
        //                         $query->where('status', 0);
        //                     }
        //                 })
        //                 ->where('client_id', $request->user()->id)
        //                 ->whereRole($role);
        //             }
    
        //             $usersCount = $users->count();
        //             if ($request->input('itemsPerPage') > 0) {
        //                 $users->limit($request->input('itemsPerPage'))
        //                 ->skip($request->input('itemsPerPage') * ($request->input('page') - 1));
        //             }
    
        //             $sortByKeys = collect($request->input('sortBy'));
        //             if ($sortByKeys->isNotEmpty()) {
        //                 $sortByMatrix = $sortByKeys->crossJoin($request->input('sortDesc'));
        //                 $sortBy = $sortByMatrix->map(function ($item, $key) {
        //                     return [
        //                         $item[0], filter_var($item[1], FILTER_VALIDATE_BOOLEAN) ? 'DESC' : 'ASC'
        //                     ];
        //                 });
        //                 $sortBy->eachSpread(function($sortCol, $sortType) use($users, $request, $role){
        //                     if($sortCol == 'full_name'){
        //                         $users->orderByRaw("CONCAT_WS(' ', name, surname) $sortType")
        //                         ->where('client_id', $request->user()->id)
        //                         ->whereRole($role);
    
        //                     }else if($sortCol == 'email') {
        //                         $users->orderByRaw("email $sortType")
        //                         ->where('client_id', $request->user()->id)
        //                         ->whereRole($role);
    
        //                     }else if($sortCol == 'info.company_name') {
        //                         $users->whereHas('info', function (Builder $query) use ($sortType, $request) {
        //                             $query->orderByRaw("company_name $sortType");
        //                         })
        //                         ->where('client_id', $request->user()->id)
        //                         ->whereRole($role);
    
        //                     }else if($sortCol == 'client_name') {
        //                         $users->orderByRaw("client_name $sortType")
        //                         ->where('client_id', $request->user()->id)
        //                         ->whereRole($role);
    
        //                     }else if($sortCol == 'role') {
        //                         $users->orderByRaw("role $sortType")
        //                         ->where('client_id', $request->user()->id)
        //                         ->whereRole($role);
    
        //                     }else if($sortCol == 'status') {
        //                         $users->orderByRaw("status $sortType")
        //                         ->where('client_id', $request->user()->id)
        //                         ->whereRole($role);
    
        //                     }else{
        //                         $users->orderBy($sortCol, $sortType)
        //                         ->where('client_id', $request->user()->id)
        //                         ->whereRole($role);
        //                     }
        //                 });
        //             }else{
        //                 $users->where('client_id', $request->user()->id)
        //                 ->whereRole($role)->latest();
        //             }
    
        //             $users = $users->get()->toArray();
        //             $data = [];
        //             $data['data'] = $users;
        //             $data['count'] = $usersCount;
            
        //             return $this->respond($data);
        //         }else {
        //             $users->where('client_id', $request->user()->client_id);
                    
        //             $searchKey = $request->input('search');
        //             if (!empty($searchKey)) {
        //                 $users->whereRaw("CONCAT_WS(' ', name, surname) like '%" . $searchKey . "%' ")
        //                 ->where('client_id', $request->user()->client_id)
        //                 ->whereRole($role);
    
        //                 $users->orWhere('email', 'LIKE', "%{$searchKey}%")
        //                 ->where('client_id', $request->user()->client_id)
        //                 ->whereRole($role);
    
        //                 $users->orWhere(function (Builder $query) use ($searchKey, $request) {
        //                     $query->whereHas('info', function (Builder $query) use ($searchKey, $request) {
        //                         $query->where('company_name', 'LIKE', "%{$searchKey}%");
        //                     });
        //                 })
        //                 ->where('client_id', $request->user()->client_id)
        //                 ->whereRole($role);
    
        //                 $users->orWhere('client_name', 'LIKE', "%{$searchKey}%")
        //                 ->where('client_id', $request->user()->client_id)
        //                 ->whereRole($role);
    
        //                 $users->orWhere('role', 'LIKE', "%{$searchKey}%")
        //                 ->where('client_id', $request->user()->client_id)
        //                 ->whereRole($role);
    
        //                 $users->orWhere(function (Builder $query) use ($searchKey, $request) {
        //                     if($searchKey === 'enable' || $searchKey === 'Enable') {
        //                         $query->where('status', 1);
        //                     }
        //                     if($searchKey == 'disable' || $searchKey == 'Disable') {
        //                         $query->where('status', 0);
        //                     }
        //                 })
        //                 ->where('client_id', $request->user()->client_id)
        //                 ->whereRole($role);
        //             }
    
        //             $usersCount = $users->count();
        //             if ($request->input('itemsPerPage') > 0) {
        //                 $users->limit($request->input('itemsPerPage'))
        //                 ->skip($request->input('itemsPerPage') * ($request->input('page') - 1));
        //             }
    
        //             $sortByKeys = collect($request->input('sortBy'));
        //             if ($sortByKeys->isNotEmpty()) {
        //                 $sortByMatrix = $sortByKeys->crossJoin($request->input('sortDesc'));
        //                 $sortBy = $sortByMatrix->map(function ($item, $key) {
        //                     return [
        //                         $item[0], filter_var($item[1], FILTER_VALIDATE_BOOLEAN) ? 'DESC' : 'ASC'
        //                     ];
        //                 });
        //                 $sortBy->eachSpread(function($sortCol, $sortType) use($users, $request, $role){
        //                     if($sortCol == 'full_name'){
        //                         $users->orderByRaw("CONCAT_WS(' ', name, surname) $sortType")
        //                         ->where('client_id', $request->user()->client_id)
        //                         ->whereRole($role);
    
        //                     }else if($sortCol == 'email') {
        //                         $users->orderByRaw("email $sortType")
        //                         ->where('client_id', $request->user()->client_id)
        //                         ->whereRole($role);
    
        //                     }else if($sortCol == 'info.company_name') {
        //                         $users->whereHas('info', function (Builder $query) use ($sortType, $request) {
        //                             $query->orderByRaw("company_name $sortType");
        //                         })
        //                         ->where('client_id', $request->user()->client_id)
        //                         ->whereRole($role);
    
        //                     }else if($sortCol == 'client_name') {
        //                         $users->orderByRaw("client_name $sortType")
        //                         ->where('client_id', $request->user()->client_id)
        //                         ->whereRole($role);
    
        //                     }else if($sortCol == 'role') {
        //                         $users->orderByRaw("role $sortType")
        //                         ->where('client_id', $request->user()->client_id)
        //                         ->whereRole($role);
    
        //                     }else if($sortCol == 'status') {
        //                         $users->orderByRaw("status $sortType")
        //                         ->where('client_id', $request->user()->client_id)
        //                         ->whereRole($role);
    
        //                     }else{
        //                         $users->orderBy($sortCol, $sortType)
        //                         ->where('client_id', $request->user()->client_id)
        //                         ->whereRole($role);
        //                     }
        //                 });
        //             }else{
        //                 $users->where('client_id', $request->user()->client_id)
        //                 ->whereRole($role)->latest();
        //             }
    
        //             $users = $users->get()->toArray();
        //             $data = [];
        //             $data['data'] = $users;
        //             $data['count'] = $usersCount;
            
        //             return $this->respond($data);
        //         }
        //     }
        // }else {
        //     if($request->input('rolealias') === 'dscclient') {
        //         $users->whereRole($role)
        //         ->where('client_id', '!=', NULL)
        //         ->where('client_name', '!=', NULL);

        //         $searchKey = $request->input('search');
        //         if (!empty($searchKey)) {
        //             $users->whereRaw("CONCAT_WS(' ', name, surname) like '%" . $searchKey . "%' ")
        //             ->whereRole($role)
        //             ->where('client_id', '!=', NULL)
        //             ->where('client_name', '!=', NULL);
    
        //             $users->orWhere('email', 'LIKE', "%{$searchKey}%")
        //             ->whereRole($role)
        //             ->where('client_id', '!=', NULL)
        //             ->where('client_name', '!=', NULL);
    
        //             $users->orWhere(function (Builder $query) use ($searchKey, $request) {
        //                 $query->whereHas('info', function (Builder $query) use ($searchKey, $request) {
        //                     $query->where('company_name', 'LIKE', "%{$searchKey}%");
        //                 });
        //             })->whereRole($role)
        //             ->where('client_id', '!=', NULL)
        //             ->where('client_name', '!=', NULL);
    
        //             $users->orWhere('client_name', 'LIKE', "%{$searchKey}%")
        //             ->whereRole($role)
        //             ->where('client_id', '!=', NULL)
        //             ->where('client_name', '!=', NULL);
    
        //             $users->orWhere('role', 'LIKE', "%{$searchKey}%")
        //             ->whereRole($role)
        //             ->where('client_id', '!=', NULL)
        //             ->where('client_name', '!=', NULL);
    
        //             $users->orWhere(function (Builder $query) use ($searchKey, $request) {
        //                 if($searchKey === 'enable' || $searchKey === 'Enable') {
        //                     $query->where('status', 1);
        //                 }
        //                 if($searchKey == 'disable' || $searchKey == 'Disable') {
        //                     $query->where('status', 0);
        //                 }
        //             })->whereRole($role)
        //             ->where('client_id', '!=', NULL)
        //             ->where('client_name', '!=', NULL);
        //         }
    
        //         $usersCount = $users->count();
        //         if ($request->input('itemsPerPage') > 0) {
        //             $users->limit($request->input('itemsPerPage'))
        //             ->skip($request->input('itemsPerPage') * ($request->input('page') - 1));
        //         }
    
        //         $sortByKeys = collect($request->input('sortBy'));
        //         if ($sortByKeys->isNotEmpty()) {
        //             $sortByMatrix = $sortByKeys->crossJoin($request->input('sortDesc'));
        //             $sortBy = $sortByMatrix->map(function ($item, $key) {
        //                 return [
        //                     $item[0], filter_var($item[1], FILTER_VALIDATE_BOOLEAN) ? 'DESC' : 'ASC'
        //                 ];
        //             });
        //             $sortBy->eachSpread(function($sortCol, $sortType) use($users, $role){
        //                 if($sortCol == 'full_name'){
        //                     $users->orderByRaw("CONCAT_WS(' ', name, surname) $sortType")
        //                     ->whereRole($role)
        //                     ->where('client_id', '!=', NULL)
        //                     ->where('client_name', '!=', NULL);
    
        //                 }else if($sortCol == 'email') {
        //                     $users->orderByRaw("email $sortType")
        //                     ->whereRole($role)
        //                     ->where('client_id', '!=', NULL)
        //                     ->where('client_name', '!=', NULL);
    
        //                 }else if($sortCol == 'info.company_name') {
        //                     $users->whereHas('info', function (Builder $query) use ($sortType) {
        //                         $query->orderByRaw("company_name $sortType");
        //                     })->whereRole($role)
        //                     ->where('client_id', '!=', NULL)
        //                     ->where('client_name', '!=', NULL);
    
        //                 }else if($sortCol == 'client_name') {
        //                     $users->orderByRaw("client_name $sortType")
        //                     ->whereRole($role)
        //                     ->where('client_id', '!=', NULL)
        //                     ->where('client_name', '!=', NULL);
    
        //                 }else if($sortCol == 'role') {
        //                     $users->orderByRaw("role $sortType")
        //                     ->whereRole($role)
        //                     ->where('client_id', '!=', NULL)
        //                     ->where('client_name', '!=', NULL);
    
        //                 }else if($sortCol == 'status') {
        //                     $users->orderByRaw("status $sortType")
        //                     ->whereRole($role)
        //                     ->where('client_id', '!=', NULL)
        //                     ->where('client_name', '!=', NULL);
    
        //                 }else{
        //                     $users->orderBy($sortCol, $sortType)
        //                     ->whereRole($role)
        //                     ->where('client_id', '!=', NULL)
        //                     ->where('client_name', '!=', NULL);
        //                 }
        //             });
        //         }else{
        //             $users->whereRole($role)
        //             ->where('client_id', '!=', NULL)
        //             ->where('client_name', '!=', NULL)
        //             ->latest();
        //         }
                
        //         $users = $users->get()->toArray();
        //         $data = [];
        //         $data['data'] = $users;
        //         $data['count'] = $usersCount;
        
        //         return $this->respond($data);

        //     }else {
        //         $searchKey = $request->input('search');
        //         if (!empty($searchKey)) {
        //             $users->whereRaw("CONCAT_WS(' ', name, surname) like '%" . $searchKey . "%' ")
        //             ->whereRole($role);
    
        //             $users->orWhere('email', 'LIKE', "%{$searchKey}%")
        //             ->whereRole($role);
    
        //             $users->orWhere(function (Builder $query) use ($searchKey, $request) {
        //                 $query->whereHas('info', function (Builder $query) use ($searchKey, $request) {
        //                     $query->where('company_name', 'LIKE', "%{$searchKey}%");
        //                 });
        //             })->whereRole($role);
    
        //             $users->orWhere('client_name', 'LIKE', "%{$searchKey}%")
        //             ->whereRole($role);
    
        //             $users->orWhere('role', 'LIKE', "%{$searchKey}%")
        //             ->whereRole($role);
    
        //             $users->orWhere(function (Builder $query) use ($searchKey, $request) {
        //                 if($searchKey === 'enable' || $searchKey === 'Enable') {
        //                     $query->where('status', 1);
        //                 }
        //                 if($searchKey == 'disable' || $searchKey == 'Disable') {
        //                     $query->where('status', 0);
        //                 }
        //             })->whereRole($role);
        //         }
    
        //         $usersCount = $users->count();
        //         if ($request->input('itemsPerPage') > 0) {
        //             $users->limit($request->input('itemsPerPage'))
        //             ->skip($request->input('itemsPerPage') * ($request->input('page') - 1));
        //         }
    
        //         $sortByKeys = collect($request->input('sortBy'));
        //         if ($sortByKeys->isNotEmpty()) {
        //             $sortByMatrix = $sortByKeys->crossJoin($request->input('sortDesc'));
        //             $sortBy = $sortByMatrix->map(function ($item, $key) {
        //                 return [
        //                     $item[0], filter_var($item[1], FILTER_VALIDATE_BOOLEAN) ? 'DESC' : 'ASC'
        //                 ];
        //             });
        //             $sortBy->eachSpread(function($sortCol, $sortType) use($users, $role){
        //                 if($sortCol == 'full_name'){
        //                     $users->orderByRaw("CONCAT_WS(' ', name, surname) $sortType")
        //                     ->whereRole($role);
    
        //                 }else if($sortCol == 'email') {
        //                     $users->orderByRaw("email $sortType")
        //                     ->whereRole($role);
    
        //                 }else if($sortCol == 'info.company_name') {
        //                     $users->whereHas('info', function (Builder $query) use ($sortType) {
        //                         $query->orderByRaw("company_name $sortType");
        //                     })->whereRole($role);
    
        //                 }else if($sortCol == 'client_name') {
        //                     $users->orderByRaw("client_name $sortType")
        //                     ->whereRole($role);
    
        //                 }else if($sortCol == 'role') {
        //                     $users->orderByRaw("role $sortType")
        //                     ->whereRole($role);
    
        //                 }else if($sortCol == 'status') {
        //                     $users->orderByRaw("status $sortType")
        //                     ->whereRole($role);
    
        //                 }else{
        //                     $users->orderBy($sortCol, $sortType)
        //                     ->whereRole($role);
        //                 }
        //             });
        //         }else{
        //             $users->whereRole($role)->latest();
        //         }
                
        //         $users = $users->get()->toArray();
        //         $data = [];
        //         $data['data'] = $users;
        //         $data['count'] = $usersCount;
        
        //         return $this->respond($data);
        //     }
        // }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $this->authorize('create', User::class);

        DB::beginTransaction();
        try {
            //Insert data into users table
            $user = new User();
            $userFillableData = $request->only($user->getFillable());
            $user->fill($userFillableData)->save();
            $user->save();

            //Insert data into user_infos table
            $userInfo = new UserInfo();
            $userInfoFillableData = collect($request->input('info'))->only($userInfo->getFillable());
            $userInfoTranslationsData = collect($request->input('info.translations'))->keyBy('locale');
            $userInfoData = $userInfoTranslationsData->merge($userInfoFillableData)->all();
            
            $userInfo->fill($userInfoData);
            $userInfo->user()->associate($user);
            $userInfo->save();

            DB::commit();

            return $this->respondCreated('User created successfully.', [
                'user' => $user,
            ]);
        } catch (ModelNotFoundException $e) {
            DB::rollback();
            return $this->respondWithExpectationFailed('Sorry, Operation Failed');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $role, $id)
    {
        $this->authorize('update', User::find($id));

        try {
            $user = User::whereId($id)->whereRole($role)->firstOrFail();
            $user->load('info', 'info.translations');

            $data = [
                'user' => $user,
            ];
            return $this->respond($data);

        } catch (\Throwable $th) {
            return $this->respondNotFound($th->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $role, $id)
    {
        $this->authorize('update', User::find($id));

        DB::beginTransaction();
        try {
            $user = User::whereId($id)->whereRole($role)->firstOrFail();
            $userFillableData = $request->only($user->getFillable());
            $user->fill($userFillableData);
            $user->save();

            //update data into user_infos table
            $userInfo = $user->info;
            $userInfoFillableData = collect($request->input('info'))->only($userInfo->getFillable());
            $userInfoTranslationsData = collect($request->input('info.translations'))->keyBy('locale');
            $userInfoData = $userInfoTranslationsData->merge($userInfoFillableData)->all();
            $userInfo->fill($userInfoData)->save();

            DB::commit();

            return $this->respondUpdated('User updated successfully.', [
                'user' => $user,
            ]);
        } catch (ModelNotFoundException $e) {
            DB::rollback();
            return $this->respondWithExpectationFailed('Sorry, Operation Failed');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $role, $id)
    {
        $this->authorize('delete', User::find($id));

        DB::beginTransaction();
        try {
            if(($request->user()->id ==1) || 
                (($request->user()->role == 'client') && 
                ($request->user()->id == User::find($id)->client_id))) {
                    
                $user = User::whereId($id)->whereRole($role)->firstOrFail();
                if( (!$user->info()->exists() || 
                    ($user->info()->exists() && $user->info()->delete())) && 
                    $user->delete()) {
                    DB::commit();
                    return $this->respond([
                        'status' => 1,
                        'message' => 'Succefully Deleted'
                    ]);
                } else {
                    DB::rollback();
                    return $this->respond([
                        'status' => 0,
                        'message' => 'Deleting Failed.'
                    ]);
                }
            }
        } catch (ModelNotFoundException $e) {
            return $this->respondNotFound('Sorry, Operation Failed');
        }
    }
}
