<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CommonController extends Controller
{
    public function test() {
        return Request()->json(["test"]);
    }
}
