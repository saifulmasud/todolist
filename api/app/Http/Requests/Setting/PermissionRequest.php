<?php

namespace App\Http\Requests\Setting;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class PermissionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->permissionRules();
    }

    private function basicPermissionRules(){
        $rules = [
            'user_id' => 'required|array',
            'permission_name' => 'required|array',
            'permission_action' => 'required|array',
            'created_by' => 'required|numeric'
        ];

        return $rules;
    }

    private function permissionRules()
    {
        $rules = $this->basicPermissionRules();
        $rules['permission_description'] = 'nullable|string';
        return $rules;
    }
}
