<?php

namespace App\Http\Requests\Activity;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

use App\Models\User\User;
use App\Models\Activity\Todo;

class TodoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->todoRules();
    }

    private function basicTodoRules() {
        $rules = [
            'todoer_id' => ['required', 'numeric'],
            'title' => ['required', 'string'],
            'start_time' => [
                'required', 
                'date_format:H:i',
                function ($attribute, $value, $fail) {
                    if(Request::has('id')) {
                        $todos = Todo::where('id', '!=', Request::input('id'))
                        ->where('todoer_id', Request::input('todoer_id'))
                        ->where('min_date', Request::input('min_date'))
                        ->where(function($query) use($value) {
                            $query->where('start_time', '<=', $value)
                            ->where('end_time', '>=', $value);

                        })->get();

                        if(isset($todos) && ($todos->count() > 0)) {
                            $fail(' Start time found to other time range on this date ');
                        }
                    }else {
                        $todos = Todo::where('todoer_id', Request::input('todoer_id'))
                        ->where('min_date', Request::input('min_date'))
                        ->where(function($query) use($value) {
                            $query->where('start_time', '<=', $value)
                            ->where('end_time', '>=', $value);

                        })->get();

                        if(isset($todos) && ($todos->count() > 0)) {
                            $fail(' Start time found to other time range on this date ');
                        }
                    }
                }
            ],
            'end_time' => [
                'required', 
                'date_format:H:i',
                'after:start_time',
                function ($attribute, $value, $fail) {
                    if(Request::has('id')) {
                        $todos = Todo::where('id', '!=', Request::input('id'))
                        ->where('todoer_id', Request::input('todoer_id'))
                        ->where('min_date', Request::input('min_date'))
                        ->where(function($query) use($value) {
                            $query->where('end_time', '>=', $value)
                            ->where('start_time', '<=', $value);

                        })->get();

                        if(isset($todos) && ($todos->count() > 0)) {
                            $fail(' End time found to other time range on this date ');
                        }
                    }else {
                        $todos = Todo::where('todoer_id', Request::input('todoer_id'))
                        ->where('min_date', Request::input('min_date'))
                        ->where(function($query) use($value) {
                            $query->where('end_time', '>=', $value)
                            ->where('start_time', '<=', $value);

                        })->get();

                        if(isset($todos) && ($todos->count() > 0)) {
                            $fail(' End time found to other time range on this date ');
                        }
                    }
                }
            ],
            'min_date' => [
                'required', 
                'date',
            ],
            'created_by' => ['required', 'numeric'],            
            'status' => ['required', 'numeric'],
        ];

        return $rules;
    }

    private function todoRules()
    {
        $rules = $this->basicTodoRules();
        return $rules;
    }

    public function messages()
    {
        return [
            'end_time.after' => ' the :attribute must be after :date  ',
        ];
    }
}
