<?php

namespace App\Http\Requests\User;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $requestForRole = Request::route()->parameter('role');
        switch ($requestForRole) {
            case 'admin':
                return $this->adminRules();
                break;
            case 'todoer':
                return $this->todoerRules();
                break;    
            default:
                return [];
                break;
        }
    }

    private function basicUserRules(){
        $requestForRole = Request::route()->parameter('role');
        $rules = [
            'name' => 'required',
            'role' => ['required', Rule::in(config('constants.roles')), 'in:'.$requestForRole],
        ];
        if(Request::isMethod('post'))
        {
            $rules['email'] = 'required|email|unique:users,email';
            $rules['mobile'] = 'required|unique:users,mobile';
            $rules['phone'] = 'nullable|sometimes';
            $rules['region_code'] = 'required|string';
            $rules['phone_region_code'] = 'nullable|sometimes|string|required_with:phone';
            $rules['password'] = 'required|min:6';
            $rules['password_confirmation'] = 'required|same:password';
        }
        else
        {
            $rules['email'] = ['required', 'email', Rule::unique('users','email')->ignore(Request::input('id'),'id')];
            $rules['mobile'] = ['required', Rule::unique('users','mobile')->ignore(Request::input('id'),'id')];
            $rules['phone'] = 'nullable|sometimes';
            $rules['region_code'] = 'required|string';
            $rules['phone_region_code'] = 'nullable|sometimes|string|required_with:phone';
            if(Request::input('password'))
            {
                $rules['password']='required|min:8';
                $rules['password_confirmation'] = 'required|same:password';
            }
        }

        return $rules;
    }

    private function adminRules()
    {
        $rules = $this->basicUserRules();
        $rules['surname'] = 'required';
        $rules['status'] = 'required';
        $rules['created_by'] = 'required|numeric';
        return $rules;
    }

    private function todoerRules()
    {
        $rules = $this->basicUserRules();
        $rules['surname'] = 'required';
        $rules['status'] = 'required';
        $rules['created_by'] = 'required|numeric';
        return $rules;
    }
}
