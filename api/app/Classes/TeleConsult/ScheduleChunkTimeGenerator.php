<?php

namespace App\Classes\TeleConsult;

use Illuminate\Support\Facades\Http;

use App\Models\User\User;

class ScheduleChunkTimeGenerator
{   
    private $timezone;

    public function __construct($request = null, $id = null) {
        $this->timezone = "Asia/Dhaka";
    }

    //Get current date
    public function getCurrDate() {
        date_default_timezone_set($this->timezone);
        $currDate = date("Y-m-d");
        return $currDate;
    }

    //Adding 1 day to provided date
    public function getNextDateOfAnyDate($date) {
        date_default_timezone_set($this->timezone);
        $timestamp = strtotime($date . " + 1 day");
        $currDate = date("Y-m-d", $timestamp);
        return $currDate;
    }
}