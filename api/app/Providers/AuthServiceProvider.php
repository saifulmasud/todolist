<?php

namespace App\Providers;

use App\Models\User\User;
use App\Policies\User\UserPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
        User::class => UserPolicy::class,
        'App\Models\Setting\Permission' => 'App\Policies\Setting\PermissionPolicy',
        'App\Models\Activity\Todo' => 'App\Policies\Activity\TodoPolicy',
        'App\Models\Dashboard\Dashboard' => 'App\Policies\Dashboard\DashboardPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
