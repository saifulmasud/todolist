<?php

namespace App\Models\User;

use DB;
use Hash;
use App\Models\User\UserInfo;
use App\Models\Setting\Permission;
use App\Models\Activity\Todo;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use SoftDeletes, HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'surname',
        'email',
        'phone',
        'mobile',
        'region_code',
        'phone_region_code',
        'role',
        'password',
        'created_by',
        'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = ['full_name'];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
        });
        static::updating(function ($model) {
        });
        static::saving(function ($model) {
        });
        static::deleting(function($user) {
            $user->permissions()->delete();
            
            foreach($user->todos as $todo) {
                $todo->info()->delete();
            }
            $user->todos()->delete();
        });
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public function getFullNameAttribute(): string
    {
        return $this->name . ($this->surname ? ' ' . $this->surname : '');
    }

    public function info(): HasOne
    {
        return $this->hasOne(UserInfo::class, 'user_id', 'id');
    }

    public function permissions(): HasMany
    {
        return $this->hasMany(Permission::class, 'user_id', 'id');
    }

    public function todos(): HasMany
    {
        return $this->hasMany(Todo::class, 'todoer_id', 'id'); 
    }
}
