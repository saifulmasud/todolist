<?php

namespace App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserInfo extends Model implements TranslatableContract 
{
    use SoftDeletes, Translatable;

    protected $table = 'user_infos';    
    protected $fillable = [
       'user_id', 
       'address',
    ];
    public $translatedAttributes = ['description'];

    public $timestamps = true;

    /**
     * Get the user that owns the UserInfo
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}