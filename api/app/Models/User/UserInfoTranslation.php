<?php

namespace App\Models\User;
use Illuminate\Database\Eloquent\Model;

class UserInfoTranslation extends Model 
{
    protected $table = 'user_info_translations';    
    protected $fillable = [
        'user_info_id', 
        'locale', 
        'description', 
        'deleted_at'
    ];

    public $timestamps = true;
}