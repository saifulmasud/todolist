<?php

namespace App\Models\Activity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class TodoInfoTranslation extends Model
{
    use SoftDeletes, HasFactory;

    protected $table = 'todo_info_translations';  

    protected $fillable = [
        'todo_info_id', 
        'locale', 
        'description', 
        'deleted_at'
    ];

    public $timestamps = true;
}
