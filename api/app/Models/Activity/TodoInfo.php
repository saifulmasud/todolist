<?php

namespace App\Models\Activity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;

class TodoInfo extends Model implements TranslatableContract 
{
    use SoftDeletes, Translatable, HasFactory;

    protected $table = 'todo_infos'; 

    protected $fillable = [
        'todo_id', 
        'deleted_at',
    ];

    public $translatedAttributes = ['description'];

    public $timestamps = true;

    public function todo(): BelongsTo
    {
        return $this->belongsTo(Todo::class, 'todo_id', 'id');
    }
}
