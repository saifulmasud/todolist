<?php

namespace App\Models\Activity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

use App\Models\User\User;
use App\Models\Activity\TodoInfo;

class Todo extends Model
{
    use SoftDeletes, HasFactory;

    protected $table = 'todos'; 

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'todoer_id',
        'title',
        'start_time',
        'end_time',
        'min_date',
        'created_by',
        'status',
        'deleted_at',
    ];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
        });
        static::updating(function ($model) {
        });
        static::saving(function ($model) {
        });
        static::deleting(function($model) {
        });
    }

    public function info(): HasOne
    {
        return $this->hasOne(TodoInfo::class, 'todo_id', 'id');
    }

    public function todoer(): BelongsTo
    {
        return $this->belongsTo(User::class, 'todoer_id', 'id');
    }
}
