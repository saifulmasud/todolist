<?php
use Illuminate\Support\Facades\Route;

Route::prefix('{locale}/v1/settings')->where(['locale' => '[a-zA-Z]{2}'])
    ->middleware(['localization', 'auth:sanctum'])
    ->namespace('Setting')
    ->group(function () {
        Route::resource('permission-todoers', 'PermissionTodoerController');
    });