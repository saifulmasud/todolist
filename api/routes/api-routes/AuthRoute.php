<?php
use Illuminate\Support\Facades\Route;

Route::prefix('{locale}/v1/auth')->where(['locale' => '[a-zA-Z]{2}'])
    ->middleware(['localization'])
    ->namespace('Auth')
    ->group(function () {
        Route::post('login', 'AuthController@login');
        Route::get('me', 'AuthController@me');
        Route::post('logout', 'AuthController@logout');
    });