<?php
use Illuminate\Support\Facades\Route;

Route::prefix('{locale}/v1/activities')->where(['locale' => '[a-zA-Z]{2}'])
    ->middleware(['localization', 'auth:sanctum'])
    ->namespace('Activity')
    ->group(function () {
        Route::resource('todos', 'TodoController');
    });
