<?php
use Illuminate\Support\Facades\Route;

Route::prefix('{locale}/v1/signingups')->where(['locale' => '[a-zA-Z]{2}'])
    ->middleware(['localization'])
    ->namespace('Signingup')
    ->group(function () {
        Route::resource('{role}s', 'SigningupController')->where([
            'role' => '^(' . implode('|', config('constants.roles')) . ')$',
            'id' => '[0-9]+'
        ])->parameters(['{role}s' => 'id'])->except(['show']); //By default, Route::resource will create the route parameters for your resource routes based on the "singularized" version of the resource name, and use it as parameter name in methods like show, update, edit and delete. thats why changing the param name with '{role}s' => 'id' so in the mentioned methods we can recieve user id's with param name id without any route error. otherwise will give route error "cannot reference variable name \"role\" more than once."".
        
    });
