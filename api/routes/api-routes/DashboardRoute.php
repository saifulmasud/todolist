<?php
use Illuminate\Support\Facades\Route;

Route::prefix('{locale}/v1/dashboards')->where(['locale' => '[a-zA-Z]{2}'])
    ->middleware(['localization', 'auth:sanctum'])
    ->namespace('Dashboard')
    ->group(function () {
        Route::resource('dashboards', 'DashboardController');
    });
