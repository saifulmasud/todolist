<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTodosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('todos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('todoer_id')->index();
            $table->string('title', 100);
            $table->time('start_time');
            $table->time('end_time');
            $table->date('min_date');
            $table->unsignedBigInteger('created_by');
            $table->tinyInteger('status')->default('1');
            $table->softDeletes('deleted_at');
            $table->timestamps();
            $table->foreign('todoer_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('todos');
    }
}
