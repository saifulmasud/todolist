<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTodoInfoTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('todo_info_translations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('todo_info_id')->index();
            $table->string('locale', 2)->index();
			$table->text('description')->nullable();
            $table->softDeletes('deleted_at');
            $table->timestamps();
            $table->foreign('todo_info_id')->references('id')->on('todo_infos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('todo_info_translations');
    }
}
