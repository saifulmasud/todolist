<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UserInfoTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_info_translations', function (Blueprint $table) {
            $table->id('id');
            $table->unsignedBigInteger('user_info_id')->index();
            $table->string('locale', 2)->index();
			$table->text('description')->nullable();	
            $table->timestamps();
            $table->foreign('user_info_id')->references('id')->on('user_infos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_info_translations');
    }
}
