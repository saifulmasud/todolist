<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\Models\User\User();
        $user = $user->create([
            'name'=>'Admin',
            'surname'=>'User',
            'email'=>'admin@todo.com',
            'password'=> 'admintodo',
            'role' => 'admin',
            'created_by' => 1,
        ]);

        $userInfo = new \App\Models\User\UserInfo();
        $userInfo->create([
            'user_id'=>$user->id,
        ]);
    }
}
