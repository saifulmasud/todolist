# todolist-api-driven-client-backend

# Backend:
	# Laravel = ^8.40
	# PHP = ^7.3|^8.0

# Frontend:
	# Nuxt Framework = ^2.15.7
	# Vuetify = ^2.5.4
	
# Node: 
	# npm = 9.4.1
	# node = 16.19.0



# Installation:

# Backend:
	# 1) Up to local or remote server
	# 2) Set .env for "APP_URL, DB_DATABASE, DB_USERNAME, DB_PASSWORD"
	# 3) Command : composer install
	# 4) Following commands:
		# php artisan view:clear
		# php artisan route:clear
		# php artisan config:clear
		# php artisan cache:clear
		# php artisan key:generate
		# composer dump-autoload
		
		# php artisan migrate
		# php artisan db:seed 
	
	


#Frontend:
	# 1) npm install
	# 2) Set .env == exactly backend .env "APP_URL" value
	# 3) nuxt.config.js => ssr: false, target: 'static'
	# 4) For local dev command : npm run dev
	# 5) For remote prod ssg command: a) npm run generate b) npm run start
	
	
	
#Security Maintained:
	# 1) CORS handling
	# 2) Frontend component based navigation guard
	# 3) Laravel Sanctum + Nuxt Auth Authentication protection
	# 4) Laravel Policy = Backend Authorization protection
	
	
	
# Design Pattern = Maintained Laravel Architectural MVC pattern folder structure. No Extra 22 gof pattern applied




# About Todo List Application
	# 1) after installing by default admin in the dB while db:seed
	# 2) Admin credential from database > seed file
	# 3) There are two users a) by default admin b) todoer
	# 4) todoer user can be registered: 
			# by admin from application inside
			# by registration link
			
	# 5) After registered 'todoer' user can't access "profile' or 'todo' CRUD
	# 6) Admin should give permission from "setting" option
	# 7) In permission setting options have to selcet registered 'todoer' and then apply permission
	# 8) Two type of permission : a) 'Profile' b) 'Todo'
	# 9) CRUD verbs 'view' and 'edit' only apply to access 'Profile', whether 'create' or 'delete' applied or not
	# 10) 'view', 'edit', 'create', 'delete' all can applied for 'todo' CRUD operation
	
#################################################################################################################



